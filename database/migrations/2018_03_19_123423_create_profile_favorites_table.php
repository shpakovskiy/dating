<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileFavoritesTable extends Migration
{
    public function up()
    {
        Schema::create('profile_favorites', function (Blueprint $table) {
            $table->integer('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->integer('profile_id')->references('id')->on('profile_profiles')->onDelete('CASCADE');
            $table->primary(['user_id', 'profile_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('profile_favorites');
    }
}
