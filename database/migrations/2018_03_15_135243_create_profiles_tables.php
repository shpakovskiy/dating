<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTables extends Migration
{
    public function up()
    {
        Schema::create('profile_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->string('title');
            $table->integer('age');
            $table->text('address');
            $table->text('content');
            $table->string('status', 16);
            $table->text('reject_reason')->nullable();
            $table->timestamps();
            $table->timestamp('published_at')->nullable();
            $table->timestamp('expires_at')->nullable();
        });



        Schema::create('profile_profile_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->references('id')->on('profile_profiles')->onDelete('CASCADE');
            $table->string('file');
        });
    }

    public function down()
    {
        Schema::dropIfExists('profile_profile_photos');
        Schema::dropIfExists('profile_profiles');
    }
}
