<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::group([
    'prefix' => 'profiles',
    'as' => 'profiles.',
    'namespace' => 'Profiles',
], function () {
    Route::get('/show/{profile}', 'profileController@show')->name('show');

    Route::post('/show/{profile}/favorites', 'FavoriteController@add')->name('favorites');
    Route::delete('/show/{profile}/favorites', 'FavoriteController@remove');

    Route::get('/{profiles_path?}', 'profileController@index')->name('index')->where('profiles_path', '.+');
});


