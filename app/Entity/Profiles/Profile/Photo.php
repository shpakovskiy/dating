<?php

namespace App\Entity\profiles\Profile;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string file
 */
class Photo extends Model
{
    protected $table = 'profile_profile_photos';

    public $timestamps = false;

    protected $fillable = ['file'];
}
