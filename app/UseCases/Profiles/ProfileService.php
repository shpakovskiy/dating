<?php

namespace App\UseCases\profiles;

use App\Entity\profiles\profile\profile;
use App\Entity\profiles\Category;
use App\Entity\Region;
use App\Entity\User\User;
use App\Events\profile\ModerationPassed;
use App\Http\Requests\profiles\AttributesRequest;
use App\Http\Requests\profiles\CreateRequest;
use App\Http\Requests\profiles\EditRequest;
use App\Http\Requests\profiles\PhotosRequest;
use App\Http\Requests\profiles\RejectRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class profileService
{
    public function create($userId, $categoryId, $regionId, CreateRequest $request): profile
    {
        /** @var User $user */
        $user = User::findOrFail($userId);
        /** @var Category $category */
        $category = Category::findOrFail($categoryId);
        /** @var Region $region */
        $region = $regionId ? Region::findOrFail($regionId) : null;

        return DB::transaction(function () use ($request, $user, $category, $region) {

            /** @var profile $profile */
            $profile = profile::make([
                'title' => $request['title'],
                'content' => $request['content'],
                'price' => $request['price'],
                'address' => $request['address'],
                'status' => profile::STATUS_DRAFT,
            ]);

            $profile->user()->associate($user);
            $profile->category()->associate($category);
            $profile->region()->associate($region);

            $profile->saveOrFail();

            foreach ($category->allAttributes() as $attribute) {
                $value = $request['attributes'][$attribute->id] ?? null;
                if (!empty($value)) {
                    $profile->values()->create([
                        'attribute_id' => $attribute->id,
                        'value' => $value,
                    ]);
                }
            }

            return $profile;
        });
    }

    public function addPhotos($id, PhotosRequest $request): void
    {
        $profile = $this->getprofile($id);

        DB::transaction(function () use ($request, $profile) {
            foreach ($request['files'] as $file) {
                $profile->photos()->create([
                    'file' => $file->store('profiles', 'public')
                ]);
            }
            $profile->update();
        });
    }

    public function edit($id, EditRequest $request): void
    {
        $profile = $this->getprofile($id);
        $profile->update($request->only([
            'title',
            'content',
            'price',
            'address',
        ]));
    }

    public function sendToModeration($id): void
    {
        $profile = $this->getprofile($id);
        $profile->sendToModeration();
    }

    public function moderate($id): void
    {
        $profile = $this->getprofile($id);
        $profile->moderate(Carbon::now());
        event(new ModerationPassed($profile));
    }

    public function reject($id, RejectRequest $request): void
    {
        $profile = $this->getprofile($id);
        $profile->reject($request['reason']);
    }

    public function editAttributes($id, AttributesRequest $request): void
    {
        $profile = $this->getprofile($id);

        DB::transaction(function () use ($request, $profile) {
            $profile->values()->delete();
            foreach ($profile->category->allAttributes() as $attribute) {
                $value = $request['attributes'][$attribute->id] ?? null;
                if (!empty($value)) {
                    $profile->values()->create([
                        'attribute_id' => $attribute->id,
                        'value' => $value,
                    ]);
                }
            }
            $profile->update();
        });
    }

    public function expire(profile $profile): void
    {
        $profile->expire();
    }

    public function close($id): void
    {
        $profile = $this->getprofile($id);
        $profile->close();
    }

    public function remove($id): void
    {
        $profile = $this->getprofile($id);
        $profile->delete();
    }

    private function getprofile($id): profile
    {
        return profile::findOrFail($id);
    }
}
