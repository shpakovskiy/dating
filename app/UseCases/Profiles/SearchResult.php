<?php

namespace App\UseCases\profiles;

use Illuminate\Contracts\Pagination\Paginator;

class SearchResult
{
    public $profiles;
    public $regionsCounts;
    public $categoriesCounts;

    public function __construct(Paginator $profiles, array $regionsCounts, array $categoriesCounts)
    {
        $this->profiles = $profiles;
        $this->regionsCounts = $regionsCounts;
        $this->categoriesCounts = $categoriesCounts;
    }
}
