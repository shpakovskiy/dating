<?php

namespace App\UseCases\profiles;

use App\Entity\profiles\profile\profile;
use App\Entity\User\User;

class FavoriteService
{
    public function add($userId, $profileId): void
    {
        $user = $this->getUser($userId);
        $profile = $this->getprofile($profileId);

        $user->addToFavorites($profile->id);
    }

    public function remove($userId, $profileId): void
    {
        $user = $this->getUser($userId);
        $profile = $this->getprofile($profileId);

        $user->removeFromFavorites($profile->id);
    }

    private function getUser($userId): User
    {
        return User::findOrFail($userId);
    }

    private function getprofile($profileId): profile
    {
        return profile::findOrFail($profileId);
    }
}
