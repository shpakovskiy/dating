<?php

namespace App\Providers;


use App\Entity\Profiles\Profile\Profile;
use App\Entity\User\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;


class AuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        //
    ];

    public function boot(): void
    {
        $this->registerPolicies();
        $this->registerPermissions();

    }

    private function registerPermissions(): void
    {
        Gate::define('manage-users', function (User $user) {
            return $user->isAdmin() || $user->isModerator();
        });


        Gate::define('manage-profiles', function (User $user) {
            return $user->isAdmin() || $user->isModerator();
        });

        Gate::define('show-profile', function (User $user, Profile $profile) {
            return $user->isAdmin() || $user->isModerator() || $profile->user_id === $user->id;
        });


    }
}

