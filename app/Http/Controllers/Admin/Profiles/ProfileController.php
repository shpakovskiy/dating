<?php

namespace App\Http\Controllers\Admin\Profiles;

use App\Entity\profiles\Profile\Profile;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\profiles\EditRequest;
use App\Http\Requests\profiles\PhotosRequest;
use App\Http\Requests\profiles\RejectRequest;
use App\UseCases\profiles\profileService;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    private $service;

    public function __construct(ProfileService $service)
    {
        $this->service = $service;
        $this->middleware('can:manage-profiles');
    }

    public function index(Request $request)
    {
        $query = Profile::orderByDesc('updated_at');

        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
        }

        if (!empty($value = $request->get('title'))) {
            $query->where('title', 'like', '%' . $value . '%');
        }

        if (!empty($value = $request->get('user'))) {
            $query->where('user_id', $value);
        }

        if (!empty($value = $request->get('status'))) {
            $query->where('status', $value);
        }

        $profiles = $query->paginate(20);

        $statuses = Profile::statusesList();

        $roles = User::rolesList();

        return view('admin.profiles.profiles.index', compact('profiles', 'statuses', 'roles'));
    }

    public function editForm(Profile $profile)
    {
        return view('profiles.edit.profile', compact('profile'));
    }

    public function edit(EditRequest $request, Profile $profile)
    {
        try {
            $this->service->edit($profile->id, $request);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('profiles.show', $profile);
    }

    public function attributesForm(Profile $profile)
    {
        return view('profiles.edit.attributes', compact('profile'));
    }

    public function photosForm(Profile $profile)
    {
        return view('profiles.edit.photos', compact('profile'));
    }

    public function photos(PhotosRequest $request, Profile $profile)
    {
        try {
            $this->service->addPhotos($profile->id, $request);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('profiles.show', $profile);
    }

    public function moderate(Profile $profile)
    {
        try {
            $this->service->moderate($profile->id);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('profiles.show', $profile);
    }

    public function rejectForm(Profile $profile)
    {
        return view('admin.profiles.profiles.reject', compact('profile'));
    }

    public function reject(RejectRequest $request, Profile $profile)
    {
        try {
            $this->service->reject($profile->id, $request);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('profiles.show', $profile);
    }

    public function destroy(Profile $profile)
    {
        try {
            $this->service->remove($profile->id);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('admin.profiles.profiles.index');
    }
}
